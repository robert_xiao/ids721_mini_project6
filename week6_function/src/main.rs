use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use log::{info, error};
use env_logger;

#[derive(Deserialize)]
struct Request {
    id: Option<String>,
}

#[derive(Serialize)]
struct Response {
    name: Option<String>,
    gender: Option<String>,
    height: Option<String>,
    weight: Option<String>,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    env_logger::init();

    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)?;

    info!("Request received, id: {:?}", request.id);

    let config = load_from_env().await;
    let client = Client::new(&config);

    match search_people(&client, request.id).await {
        Ok(response) => {
            info!("Successfully located personnel information. It should be in the CloudWatch logs");
            Ok(json!(response))
        },
        Err(e) => {
            error!("An error occurred while processing the request: {}. It should be in the CloudWatch logs", e);
            Err(e)
        }
    }
}

async fn search_people(client: &Client, id: Option<String>) -> Result<Response, LambdaError> {
    let table_name = "people_information";

    match id {
        Some(id_val) => {
            let result = client.get_item()
                .table_name(table_name)
                .key("id", AttributeValue::S(id_val))
                .send()
                .await?;

            match result.item {
                Some(item) => {
                    let name = item.get("name").and_then(|val| val.as_s().ok()).map(|s| s.to_string());
                    let gender = item.get("gender").and_then(|val| val.as_s().ok()).map(|s| s.to_string());
                    let height = item.get("height").and_then(|val| val.as_s().ok()).map(|s| s.to_string());
                    let weight = item.get("weight").and_then(|val| val.as_s().ok()).map(|s| s.to_string());

                    Ok(Response { name, gender, height, weight })
                },
                None => {
                    error!("No matches found. It should be in the CloudWatch logs");
                    Err(LambdaError::from("No matching person found"))
                }
            }
        },
        None => {
            error!("Missing id in request. It should be in the CloudWatch logs");
            Err(LambdaError::from("ID is required"))
        }
    }
}
