# IDS721_mini_project6
## Overview:
Instrument a Rust Lambda Function with Logging and Tracing.
## Requirement:
1. Add logging to a Rust Lambda function<br>
2. Integrate AWS X-Ray tracing<br>
3. Connect logs/traces to CloudWatch<br>
## Steps:
1.	Use the one-line command: cargo lambda new week6_function to create a new lambda function folder called week6_function.<br>
2.	Based on the week5_function, I added log and env_logger to the Rust Lambda function. The code is shown below.<br>
![Alt text](./image/code2.png) 
![Alt text](./image/code1.png)
3.	Configure the cargo.toml file as follows.<br>
4.	Add role permissions as shown below:<br>
![Alt text](./image/role.png)
 
5.	Deploy the lambda to the AWS cloud server with cargo lambda build --release and cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::905418421277:role/IDS721_lambda_week6<br>
6.	Find Monitoring and operations tools on the left side of the configuration section under the lambda function service and Click the Edit button next to Additional monitoring tools.<br>
![Alt text](./image/EditButton.png)
 
7.	Enable the AWS X-Ray and CloudWatch Lambda Insights. <br>
![Alt text](./image/Enable_X_C.png)<br>
## Display:
I can test the service's functionality using Postman to send POST requests like the following. <br>
![Alt text](./image/postman.png)
 
After using Postman to send POST requests, I can use AWS CloudWatch and AWS X-Ray consoles to monitor and analyze function performance and troubleshoot issues through logging information.<br>
#### Access X-Ray traces through the AWS X-Ray consoles:<br>
Traces:
![Alt text](./image/X_ray1.png)
![Alt text](./image/X_ray2.png) 
![Alt text](./image/X_ray3.png) 

Trace Map:
![Alt text](./image/X_ray_map.png)
#### Access logs through the AWS CloudWatch:<br>
Log groups:<br>
![Alt text](./image/cloudwatch1.png)

Logs Insights:<br>
![Alt text](./image/cloudwatch2.png)
#### Incorrect Lambda Test:<br>
Test case:<br>
![Alt text](./image/image-11.png)
Result:<br>
![Alt text](./image/image-12.png)
#### Correct Lambda Test:<br>
Test case:<br>
 ![Alt text](./image/image-13.png)
Results:<br>
 ![Alt text](./image/image-14.png)

